package cog.java.http;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Locale;

public class CogHttpError {
	private String message = "Generic Error";
	private int status;
	private String response;
	private Exception ex;

	public CogHttpError() {}

	public CogHttpError(String msg) {
		setMessage(msg);
	}

	public CogHttpError(String msg, int statusCode) {
		this.message = msg;
		this.status = statusCode;
	}

	public CogHttpError(Exception ex) {
		setException(ex);
	}

	public CogHttpError(Exception ex, int statusCode) {
		setException(ex);
		setStatusCode(statusCode);
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponse() {
		return this.response;
	}

	public boolean hasResponse() {
		return response != null && !response.equals("");
	}

	/**
	 * Gets a user-friendly message associated with this error object.
	 * 
	 * @return
	 */
	public String getFriendlyMessage() {
		return _getMessage(true);
	}

	private String _getMessage(boolean friendly) {
		if (status > 0) {
			switch (status) {
				case 300:
				case 301:
				case 302:
				case 303:
				case 304:
				case 305:
				case 306:
				case 307:
				case 308:
					return friendly ? "The request might be improperly routed. Please check your connection."
						: "REDIRECT ERROR: " + this.message;

				case 400:
					return friendly ? "Something was wrong with the request." : "BAD REQUEST";

				case 401:
					return friendly ? "The server did not allow access" : "UNAUTHORIZED";

				case 403:
					return friendly ? "The server did not allow access" : "FORBIDDEN";

				case 404:
					return friendly ? "The resource for the request could not be found" : "NOT FOUND";

				case 405:
					return friendly ? "The method for the request is not allowed" : "METHOD NOT ALLOWED";

				case 407:
					return friendly ? "Authentication required" : "PROXY AUTHENTICATION REQUIRED";

				case 408:
					return friendly ? "The connection timed out" : "REQUEST TIMEOUT";

				case 500:
					return friendly ? "An error occured." : "SERVER ERROR";

				case 501:
					return friendly ? "The method for the request is not implemented" : "NOT IMPLEMENTED";

				case 503:
					return friendly ? "Service unavailable" : "SERVICE UNAVAILABLE";

				case 504:
					return friendly ? "The connection timed out" : "GATEWAY TIMEOUT";

				default:
					if (ex != null) {
						if (ex instanceof IOException || ex instanceof UnknownHostException) {
							return friendly ? "A possible network problem has occurred. Please check your connection and try again."
								: "IO Exception";
						}

						if (ex instanceof SocketException) {
							return friendly ? "There is a connection problem." : "Socket Exception";
						}
					}
					return friendly ? "An error occured" : "AN ERROR OCCURRED";
			}
		}

		return message;
	}

	/**
	 * Checks if the request was redirected. This may not be considered as an
	 * error.
	 * 
	 * @return
	 */
	public boolean isRedirectionError() {
		int code = getStatusCode();
		return code >= 300 && code <= 308;
	}

	/**
	 * Checks if there was an authentication error with the request.
	 * 
	 * @return
	 */
	public boolean isAuthError() {
		int code = getStatusCode();
		return code == 403 || code == 401;
	}

	/**
	 * Checks if the connection/request timed out.
	 * 
	 * @return
	 */
	public boolean isTimeout() {
		int code = getStatusCode();
		return code == 408 || code == 504
			|| (message != null && message.toLowerCase(Locale.getDefault()).contains("timeout"));
	}

	/**
	 * Gets the message associated with this error. This may not be a
	 * user-friendly message.
	 * 
	 * @return
	 */
	public String getMessage() {
		return _getMessage(false);
	}

	/**
	 * Sets the message associated with this error.
	 * 
	 * @param message
	 */
	public void setMessage(String message) {

		this.message = message;
	}

	public String getMessageAndStatus() {
		return getMessage() + " [" + this.status + "]";
	}

	public String getMessageStack() {
		StringBuilder sb = new StringBuilder(getMessage());
		sb.append(" [").append(getStatusCode()).append("]");
		sb.append("\r\n").append(getResponse());
		return sb.toString();
	}

	public int getStatusCode() {
		return status;
	}

	public void setStatusCode(int status) {
		this.status = status;
	}

	public Exception getException() {
		return ex;
	}

	public void setException(Exception ex) {
		this.ex = ex;

		String prefix = "";
		if (ex instanceof SocketException) {
			prefix = "[Socket Exception] ";
		}
		else if (ex instanceof IOException) {
			prefix = "[IO Exception] ";
		}

		this.message = prefix + ex.getMessage();
	}

	public String getExceptionMessage() {
		if (ex != null) {
			return ex.getMessage();
		}
		return null;
	}

	public boolean isIOError() {
		if (this.ex != null && this.ex instanceof IOException) {
			return true;
		}
		return false;
	}

	public boolean isSocketError() {
		if (this.ex != null && this.ex instanceof SocketException) {
			return true;
		}
		return false;
	}

	/*
	 switch (status)
		{
			case 400:
				return "BAD REQUEST";
			case 401:
				return "UNAUTHORIZED";
			case 403:
				return "FORBIDDEN";
			case 404:
				return "NOT FOUND";
			case 405:
				return "METHOD NOT ALLOWED";
			case 407:
				return "PROXY AUTHENTICATION REQUIRED";
			case 408:
				return "REQUEST TIMEOUT";
			case 500:
				return "SERVER ERROR";
			case 501:
				return "NOT IMPLEMENTED";
			case 503:
				return "SERVICE UNAVAILABLE";
			case 504:
				return "GATEWAY TIMEOUT";
			default:
				return "AN ERROR OCCURRED";
		}
	 */
}
