package cog.java.http;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLConnection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONObject;

public class CogHttpMultipartRequest extends CogHttpPostRequest {
	protected Map<String, File> filesMap = new LinkedHashMap<String, File>();
	protected static final String LINE_FEED = "\r\n";
	protected static final String HYPHENS = "--";
	protected static final String BOUNDARY = "cI973tv5";

	protected Map<String, String> contentTypes = new LinkedHashMap<String, String>();
	protected String contentTypeSecondary = CogHttpContentType.TEXT.getValue();

	public CogHttpMultipartRequest(String url, Map<String, String> data, Map<String, String> filesMap)
			throws IOException {
		this(url, filesMap);
		setData(data);
	}

	public CogHttpMultipartRequest(String url, Map<String, String> filesMap) throws IOException {
		super(url);
		this.filesMap = new LinkedHashMap<String, File>();
		setFileParts(filesMap);
		init();
	}

	protected void init() {
		setContentType(CogHttpContentType.MULTIPART_FORM_DATA.getValue() + ";boundary=" + BOUNDARY);
	}

	/**
	 * Sets the file parts for the multipart request.
	 * 
	 * @param filesMap
	 *            A map of the filenames and file paths.
	 * @throws FileNotFoundException
	 */
	public void setFileParts(Map<String, String> filesMap) throws FileNotFoundException {
		if (filesMap == null)
			throw new FileNotFoundException("No file paths specified.");

		String path;
		File file;
		for (String fileName : filesMap.keySet()) {
			path = filesMap.get(fileName);
			if (!strOK(path)) {
				throw new FileNotFoundException("No file path specified.");
			}

			file = new File(path);
			if (!file.exists()) {
				throw new FileNotFoundException("File not found.");
			}

			this.filesMap.put(fileName, file);
		}
	}

	/**
	 * Adds a 'file' part for the multipart request.
	 * 
	 * @param name
	 *            The content name.
	 * @param filePath
	 *            The file path.
	 * @throws FileNotFoundException
	 */
	public void addFilePart(String name, String filePath) throws FileNotFoundException {
		if (filesMap == null) {
			filesMap = new LinkedHashMap<String, File>();
		}

		if (!strOK(filePath)) {
			throw new FileNotFoundException("File not found.");
		}

		File file = new File(filePath);
		if (!file.exists()) {
			throw new FileNotFoundException("File not found.");
		}

		filesMap.put(name, file);
	}

	/**
	 * Checks if the multipart request has 'file' parts.
	 * 
	 * @return
	 */
	public boolean hasFileParts() {
		return filesMap != null && !filesMap.isEmpty();
	}

	@Override
	public void setData(Map<String, String> params) {
		super.setData(params);
		for (String key : params.keySet()) {
			/*
			 * Set default content type for the data parts.
			 */
			contentTypes.put(key, contentTypeSecondary);
		}
	}

	/**
	 * Sets the request data as a part of the request.
	 * 
	 * @param params
	 *            The request data parameter map.
	 * @param contentType
	 *            The content type to use for each item in the parameter map
	 *            specified.
	 */
	public void setData(Map<String, String> params, CogHttpContentType contentType) {
		contentTypeSecondary = contentType.getValue();
		setData(params);
	}

	/**
	 * Sets the request data as a part of the request.
	 * 
	 * @param params
	 *            The request data parameter map.
	 * @param contentType
	 *            The content type to use for each item in the parameter map
	 *            specified.
	 */
	public void setData(Map<String, String> params, String contentType) {
		contentTypeSecondary = contentType;
		setData(params);
	}

	@Override
	public void setJsonData(JSONObject json) {
		super.setJsonData(json);
		contentTypeSecondary = CogHttpContentType.JSON.getValue();
	}

	@Override
	public void addParameter(String name, String value) {
		super.addParameter(name, value);
		contentTypes.put(name, contentTypeSecondary);
	}

	public void addParameter(String name, String value, CogHttpContentType contentType) {
		super.addParameter(name, value);
		contentTypes.put(name, contentType.getValue());
	}

	@Override
	public void setContentType(CogHttpContentType contentType) {
		this.contentTypeSecondary = contentType.getValue();
		this.contentType = CogHttpContentType.MULTIPART_FORM_DATA.getValue();
	}

	@Override
	public void send() {
		send(null, null);
	}

	@Override
	public void send(ICogHttpResponse resp) {
		send(resp, null);
	}

	@Override
	public void send(final ICogHttpResponse response_handler, final ICogHttpError error_handler) {

		if (!strOK(urlString))
			return;

		normalizeData();

		this.responseHandler = response_handler;
		this.errorHandler = error_handler;

		new Thread(new Runnable() {
			public void run() {
				pause();
				try {
					initConnectionCommons();

					if (hasFileParts() || hasData()) {
						conn.setDoOutput(true);
						conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);

						out = new DataOutputStream(conn.getOutputStream());
						PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));

						if (hasData()) {
							if (hasDataMap()) {
								for (String key : dataMap.keySet()) {
									String content = dataMap.get(key);

									writer.append(HYPHENS + BOUNDARY).append(LINE_FEED);
									writer.append("Content-Disposition: form-data; name=\"" + key + "\"")
											.append(LINE_FEED);
									// writer.append(
									// "Content-Type: "
									// + contentTypes.get(key)
									// + "; charset=UTF-8").append(
									// LINE_FEED);
									writer.append(LINE_FEED);
									writer.append(content).append(LINE_FEED);
									writer.flush();
								}
							}

							else if (hasJsonData()) {
								String content = getDataString();

								writer.append(HYPHENS + BOUNDARY).append(LINE_FEED);
								writer.append("Content-Disposition: form-data; name=\"data\"").append(LINE_FEED);
								writer.append("Content-Type: " + CogHttpContentType.JSON.getValue() + "; charset=UTF-8")
										.append(LINE_FEED);
								writer.append(LINE_FEED);
								writer.append(content).append(LINE_FEED);
								writer.flush();
							}
						}

						if (hasFileParts()) {
							String fileTag = "photo";
							File file;

							for (String fileName : filesMap.keySet()) {
								file = filesMap.get(fileName);
								fileTag = fileName;

								writer.append(HYPHENS).append(BOUNDARY).append(LINE_FEED);
								writer.append("Content-Disposition: form-data; name=\"" + fileTag + "\"; filename=\""
										+ fileName + "\"").append(LINE_FEED);
								writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName))
										.append(LINE_FEED);
								writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
								writer.append(LINE_FEED);
								writer.flush();

								FileInputStream inputStream = new FileInputStream(file);
								byte[] buffer = new byte[inputStream.available()];
								int bytesRead = -1;
								while ((bytesRead = inputStream.read(buffer)) != -1) {
									out.write(buffer, 0, bytesRead);
								}
								out.flush();
								inputStream.close();

								writer.append(LINE_FEED);
								writer.flush();
							}
						}

						writer.append(LINE_FEED).flush();
						writer.append(HYPHENS + BOUNDARY + HYPHENS).append(LINE_FEED);
						writer.close();

						out.flush();
						out.close();
					}

					/*
					 * Get and handle the response.
					 */
					conn.connect();
					in = conn.getInputStream();
					handleResponse(conn, in);
				} catch (MalformedURLException e) {
					e.printStackTrace();
					setError_bad_url();
				} catch (SocketTimeoutException e) {
					e.printStackTrace();
					setError_timeout();
				} catch (SocketException e) {
					e.printStackTrace();
					setError_socket(e);
				} catch (IOException e) {
					e.printStackTrace();
					setError_io(e);
				} finally {
					try {
						in.close();
						conn.disconnect();
					} catch (NullPointerException e) {
					} catch (IOException e) {
					}
				}

				handleError(error_handler);
			}
		}).start();
	}
}
