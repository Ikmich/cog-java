package cog.java.http;

public class CogHttpPostRequest extends CogHttpRequest {
	public CogHttpPostRequest(String url) {
		super(url);
		setRequestMethod(POST);
	}
}
