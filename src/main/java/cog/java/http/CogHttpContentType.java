package cog.java.http;

public enum CogHttpContentType {
	URL_ENCODED("application/x-www-form-urlencoded"), TEXT("text/plain"), JSON("application/json"), XML(
			"text/xml"), MULTIPART_FORM_DATA("multipart/form-data");

	private String value;

	CogHttpContentType(String contentType) {
		this.value = contentType;
	}

	public String getValue() {
		return value;
	}
}
