package cog.java.http;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cog.java.util.time.Sec;

/**
 * Base class for cog Http request utility.
 * 
 * @author ikenna.agbasimalo
 * 
 */
public abstract class CogHttpRequest {

	protected String urlString;
	protected String dataString;
	protected String responseType = "text/plain";
	protected Map<String, String> dataMap = new LinkedHashMap<String, String>();
	protected JSONObject jsonData;
	protected boolean flag_handleRawResponse = false;

	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String PUT = "PUT";
	public static final String DELETE = "DELETE";
	
	protected String requestMethod = GET; // Default request method is 'GET'
	protected int contentLength = 0;
	protected String contentType = CogHttpContentType.URL_ENCODED.getValue();
	protected int timeout = 0;
	protected boolean followsRedirects = false;
	protected boolean allowsUserInteraction = true;

	protected InputStream in;
	protected InputStreamReader inReader;
	protected HttpURLConnection conn;
	protected DataOutputStream out;
	protected CogHttpError cogHttpError;
	protected ICogHttpError errorHandler;
	protected ICogHttpResponse responseHandler;

	protected CogHttpRequest(String url) {
		urlString = url;
	}

	protected CogHttpRequest(String url, Map<String, String> params) {
		this(url);
		setData(params);
	}

	protected CogHttpRequest(String url, JSONObject jsonData) {
		this(url);
		setJsonData(jsonData);
	}

	/**
	 * Sets the request method.
	 * 
	 * @param method
	 */
	protected void setRequestMethod(String method) {
		if (strOK(method.trim())) {
			this.requestMethod = method.trim().toUpperCase();
		}
	}

	public boolean isGET() {
		return this.requestMethod.equals("GET");
	}

	public boolean isPOST() {
		return this.requestMethod.equals("POST");
	}
	
	public boolean isPUT() {
		return this.requestMethod.equals("PUT");
	}
	
	public boolean isDELETE() {
		return this.requestMethod.equals("DELETE");
	}

	protected String urlEncode(String s) {
		return urlEncode(s, null);
	}

	protected String urlEncode(String s, String encoding) {
		if (encoding == null || encoding.equals("")) {
			encoding = "UTF-8";
		}

		try {
			return URLEncoder.encode(s, encoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return s;
		}
	}

	/**
	 * Sets the request data as "application/x-www-form-url-encoded" content
	 * type.
	 * 
	 * @param params
	 */
	public void setData(Map<String, String> params) {
		if (params == null) {
			return;
		}

		this.dataMap = params;
		setDataString(buildDataStringFrom(this.dataMap));

		// Set the content type.
		setContentType(CogHttpContentType.URL_ENCODED);
	}

	/**
	 * Sets the request data as a JSON object. This assumes and automatically
	 * sets the content type to "application/json".
	 * 
	 * @param json
	 */
	public void setJsonData(JSONObject json) {
		if (json == null) {
			return;
		}
		this.jsonData = json;
		setDataString(buildDataStringFrom(this.jsonData));

		/*
		 * Set the content type
		 */
		setContentType(CogHttpContentType.JSON);
	}

	/**
	 * Adds a request parameter to the data map.
	 * 
	 * @param name
	 *        The parameter name.
	 * @param value
	 *        The parameter value.
	 */
	public void addParameter(String name, String value) {
		if (dataMap == null) {
			dataMap = new LinkedHashMap<String, String>();
		}

		dataMap.put(name, value);
		setDataString(buildDataStringFrom(dataMap));
	}

	/**
	 * Builds the data string to be used as the request data, from a Map object.
	 * 
	 * @param dataMap
	 * @return
	 */
	protected String buildDataStringFrom(Map<String, String> dataMap) {
		if (dataMap == null) {
			return "";
		}

		StringBuilder sb = new StringBuilder("");
		int i = 0;
		int lastIndex = dataMap.size() - 1;

		for (Entry<String, String> entry : dataMap.entrySet()) {
			// Encode the key and value entities.
			String key = urlEncode(entry.getKey());
			String value = urlEncode(entry.getValue());

			sb.append(key).append("=").append(value);
			if (i < lastIndex) {
				sb.append("&");
			}
			i++;
		}
		return sb.toString();
	}

	protected String buildDataStringFrom(JSONObject json) {
		if (json == null) {
			return "";
		}
		return json.toString();
	}

	protected void setDataString(String dataString) {
		this.dataString = dataString;
		updateContentLength();
	}

	protected void updateContentLength() {
		contentLength = this.dataString.trim().getBytes().length;
	}

	/**
	 * Sets the content type for the request data content.
	 * 
	 * @param contentType
	 */
	public void setContentType(CogHttpContentType contentType) {
		this.contentType = contentType.getValue();
	}

	/**
	 * Sets the content type for the request data content.
	 * 
	 * @param contentType
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Sets the content type and the response type to json.
	 */
	public void useJsonContentAndResponse() {
		setContentType(CogHttpContentType.JSON);
		setResponseType(CogHttpResponseType.JSON);
	}

	/**
	 * Checks if this request has data content to be sent.
	 * 
	 * @return
	 */
	public boolean hasData() {
		return (dataString != null && !dataString.equals("")) || hasDataMap() || hasJsonData();
	}

	protected boolean hasDataMap() {
		return dataMap != null && !dataMap.isEmpty();
	}

	protected boolean hasJsonData() {
		return jsonData != null && jsonData.length() > 0;
	}

	/**
	 * Returns the content length of the request data content.
	 * 
	 * @return
	 */
	public int getContentLength() {
		return contentLength;
	}

	/**
	 * Checks if the request data is a json content type.
	 * 
	 * @return
	 */
	public boolean isJsonContentType() {
		return contentType.equals(CogHttpContentType.JSON.getValue());
	}

	/**
	 * Checks if the request data is a text content type.
	 * 
	 * @return
	 */
	public boolean isTextContentType() {
		return contentType.equals(CogHttpContentType.TEXT.getValue());
	}

	protected String getDataString() {
		return dataString;
	}

	/**
	 * Normalizes the data to one usable data string format, taking into account
	 * the different methods used to add data.
	 */
	protected void normalizeData() {
		if (isJsonContentType()) {
			if (hasJsonData()) {
				setDataString(buildDataStringFrom(jsonData));
			}
			else if (hasDataMap()) {
				this.jsonData = new JSONObject(this.dataMap);
				setDataString(buildDataStringFrom(jsonData));
			}
			return;
		}

		if (hasDataMap()) {
			setDataString(buildDataStringFrom(this.dataMap));
			return;
		}
	}

	/**
	 * Sets the expected response type.
	 * 
	 * @param responseType
	 */
	public void setResponseType(CogHttpResponseType responseType) {
		this.responseType = responseType.getValue();
	}
	
	/**
	 * Sets the expected response type.
	 * 
	 * @param responseType
	 */
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	/**
	 * Set whether to follow redirects or not.
	 * 
	 * @param follow
	 */
	public void setRedirectFollowStatus(boolean follow) {
		this.followsRedirects = follow;
	}

	/**
	 * Checks the status of whether the connection follows redirects or not.
	 * 
	 * @return
	 */
	public boolean getRedirectFollowStatus() {
		return this.followsRedirects;
	}

	/**
	 * Sets whether to allow user interaction or not.
	 * 
	 * @param allow
	 */
	public void setAllowUserInteractionStatus(boolean allow) {
		this.allowsUserInteraction = allow;
	}

	/**
	 * Checks the status of whether the connection allows user interaction or
	 * not.
	 * 
	 * @return
	 */
	public boolean getAllowUserInteractionStatus() {
		return this.allowsUserInteraction;
	}

	/**
	 * Sets the object to handle an error if it occurs.
	 * 
	 * @param errorHandler
	 */
	public void setErrorHandler(ICogHttpError errorHandler) {
		this.errorHandler = errorHandler;
	}

	/**
	 * Sets the object to handle the response.
	 * 
	 * @param responseHandler
	 */
	public void setResponseHandler(ICogHttpResponse responseHandler) {
		this.responseHandler = responseHandler;
	}

	/**
	 * Returns the request timeout in milliseconds.
	 * 
	 * @return
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Sets the request timeout in milliseconds.
	 * 
	 * @param millis
	 */
	public void setTimeout(int millis) {
		timeout = millis;
	}

	/**
	 * Sets the request timeout in seconds.
	 * 
	 * @param seconds
	 */
	public void setTimeoutSeconds(int seconds) {
		setTimeout((int) Sec.toMillis(seconds));
	}

	/**
	 * Experimental.
	 */
	public void cancel() {
		try {
			if (inReader != null) {
				inReader.close();
			}
		} catch (Exception e) {}

		try {
			if (out != null) {
				out.close();
			}
		} catch (Exception e) {}

		try {
			if (conn != null) {
				conn.disconnect();
			}
		} catch (Exception e) {}
	}

	/**
	 * Performs the common tasks to setup a HttpURLConnection before sending the
	 * request.
	 * 
	 * @throws IOException
	 */
	protected void initConnectionCommons() throws IOException {
		URL url = new URL(urlString);
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(requestMethod);
		conn.setAllowUserInteraction(this.getAllowUserInteractionStatus());
		conn.setInstanceFollowRedirects(this.getRedirectFollowStatus());
		conn.setUseCaches(false);
		conn.setRequestProperty("Connection", "Keep-Alive");

		if (timeout > 0) {
			conn.setConnectTimeout(timeout);
		}
		else {
			/* Use default timeout. */
		}

		HttpURLConnection.setFollowRedirects(followsRedirects);
	}

	/**
	 * Sends the HTTP request.
	 */
	public void send() {
		send(null, null);
	}

	public void send(ICogHttpResponse response_handler) {
		send(response_handler, false);
	}

	/**
	 * Sends the HTTP request.
	 * 
	 * @param response_handler
	 *        The response handler.
	 * @param handleRawResponse
	 *        Whether to handle the raw response. The connection InputStream
	 *        will be passed as an argument to the response handler's
	 *        onResponse() method.
	 */
	public void send(ICogHttpResponse response_handler, boolean handleRawResponse) {
		send(response_handler, null, handleRawResponse);
	}

	/**
	 * Sends the HTTP request.
	 * 
	 * @param response_handler
	 *        The response handler.
	 * @param error_handler
	 *        The error handler if an error occurs.
	 */
	public void send(final ICogHttpResponse response_handler, final ICogHttpError error_handler) {
		send(response_handler, error_handler, false);
	}

	/**
	 * Sends the HTTP request.
	 * 
	 * @param response_handler
	 *        The response handler.
	 * @param error_handler
	 *        The error handler if an error occurs.
	 * @param handleRawResponse
	 *        Whether to handle the raw response. The connection InputStream
	 *        will be passed as an argument to the response handler's
	 *        onResponse() method.
	 */
	public void send(final ICogHttpResponse response_handler, final ICogHttpError error_handler,
		final boolean handleRawResponse) {
		if (!strOK(urlString)) {
			return;
		}

		flag_handleRawResponse = handleRawResponse;
		normalizeData();

		this.responseHandler = response_handler;
		this.errorHandler = error_handler;

		new Thread(new Runnable() {
			public void run() {
				// pause();
				try {
					if (isGET() && hasData()) {
						/*
						 * If URL already contains "?", suffix the querystring
						 * data beginning with "&".
						 */
						String sym = "?";
						if (urlString.contains(sym)) {
							sym = "&";
						}
						urlString += sym + dataString;
					}

					initConnectionCommons();

					if (!isGET()) {
						if (hasData()) {
							conn.setRequestProperty("Content-Length", String.valueOf(getContentLength()));
							conn.setRequestProperty("Content-Type", contentType);
							conn.setDoOutput(true);

							out = new DataOutputStream(conn.getOutputStream());
							out.writeBytes(getDataString());
							out.flush();
							out.close();
						}
					}

					/*
					 * Get and handle the response.
					 */
					conn.connect();

					int status = conn.getResponseCode();
					if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {
						in = conn.getErrorStream();
					}
					else {
						in = conn.getInputStream();
					}

					if (handleRawResponse) {
						if (responseHandler != null) {
							responseHandler.onResponse(in);
						}
					}
					else {
						handleResponse(conn, in);
					}

					return;
				} catch (MalformedURLException e) {
					e.printStackTrace();
					setError_bad_url();
				} catch (SocketTimeoutException e) {
					e.printStackTrace();
					setError_timeout();
				} catch (SocketException e) {
					e.printStackTrace();
					setError_socket(e);
				} catch (IOException e) {
					e.printStackTrace();
					setError_io(e);
				} finally {
					try {
						in.close();
						conn.disconnect();
					} catch (NullPointerException e) {} catch (IOException e) {}
				}

				handleError(error_handler);
			}
		}).start();
	}

	/**
	 * Handles the request response.
	 * 
	 * @param conn
	 *        The HttpURLConnection object.
	 * @param in
	 *        The response InputStream.
	 * @throws IOException
	 */
	protected void handleResponse(HttpURLConnection conn, InputStream in) throws IOException {
		if (in == null) {
			cogHttpError = new CogHttpError("No Response", 0);
		}
		else {
			int statusCode = conn.getResponseCode();
			String statusMessage = conn.getResponseMessage();

			if (flag_handleRawResponse || responseType.equals(CogHttpResponseType.BINARY.getValue())) {
				/*
				 * Do nothing with the inputstream. Send it to the response
				 * handler as it came.
				 */
				if (responseHandler != null) {
					responseHandler.onResponse(in);
				}
				return;
			}

			String responseString = readInputStream(in);

			switch (statusCode) {
				case HttpURLConnection.HTTP_OK:
					if (in != null && responseHandler != null) {
						if (responseType.equals(CogHttpResponseType.TEXT.getValue())) {
							responseHandler.onResponse(responseString);
						}

						else if (responseType.equals(CogHttpResponseType.JSON.getValue())) {
							try {
								JSONObject json = new JSONObject(responseString);
								responseHandler.onResponse(json);
							} catch (JSONException e) {
								// Improper/invalid json string.
								e.printStackTrace();
								cogHttpError = new CogHttpError(e.getMessage());
							}
						}

						else if (responseType.equals(CogHttpResponseType.JSON_ARRAY.getValue())) {
							try {
								JSONArray jsonArr = new JSONArray(responseString);
								responseHandler.onResponse(jsonArr);
							} catch (JSONException e) {
								e.printStackTrace();
								cogHttpError = new CogHttpError(e.getMessage());
							}
						}
					}
					break;

				default:
					cogHttpError = new CogHttpError(statusMessage, statusCode);
					cogHttpError.setResponse(responseString);
					break;
			}
		}

		handleError(errorHandler);
	}

	protected String readInputStream(InputStream in) throws IOException {
		String contents = "";
		Scanner sc = new Scanner(in);
		sc.useDelimiter("\\A");
		try {
			contents = sc.hasNext() ? sc.next() : "";
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			sc.close();
		}

		return contents;
	}

	protected void setError_bad_url() {
		cogHttpError = new CogHttpError("Bad URL");
	}

	protected void setError_timeout() {
		int status = 0;
		try {
			status = conn.getResponseCode();
			if (status < 1) {
				status = HttpURLConnection.HTTP_CLIENT_TIMEOUT;
			}
		} catch (IOException e1) {}
		cogHttpError = new CogHttpError("Connection Timeout", status);
	}

	protected void setError_socket(SocketException e) {
		int status = 0;
		try {
			status = conn.getResponseCode();
		} catch (IOException e1) {}
		cogHttpError = new CogHttpError(e, status);
	}

	protected void setError_io(IOException e) {
		int status = 0;
		try {
			status = conn.getResponseCode();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		cogHttpError = new CogHttpError(e, status);
	}

	protected void handleError(ICogHttpError errorHandler) {
		if (cogHttpError != null && errorHandler != null) {
			errorHandler.onError(cogHttpError);
		}
	}

	protected boolean strOK(String s) {
		return s != null && !s.equals("");
	}

	protected void pause() {
		try {
			Thread.sleep(650);
		} catch (InterruptedException e) {}
	}
}
