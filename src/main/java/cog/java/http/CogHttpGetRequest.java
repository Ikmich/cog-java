package cog.java.http;

public class CogHttpGetRequest extends CogHttpRequest {

	public CogHttpGetRequest(String url) {
		super(url);
		setRequestMethod(GET);
		setContentType(CogHttpContentType.URL_ENCODED.getValue());
	}
}
