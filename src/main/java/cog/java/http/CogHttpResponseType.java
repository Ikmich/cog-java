package cog.java.http;

public enum CogHttpResponseType {
	TEXT("text/plain"), JSON("application/json"), XML("text/xml"), JSON_ARRAY("application/json"), BINARY(
		"application/octet-stream");

	private String value;

	private CogHttpResponseType() {}

	private CogHttpResponseType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
