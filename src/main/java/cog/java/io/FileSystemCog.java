package cog.java.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for working with the file system.
 * 
 * @author ikmich
 * 
 */
public class FileSystemCog {
	private static FileSystemCog instance;

	private FileSystemCog() {}

	public static FileSystemCog getInstance() {
		if (instance == null) {
			instance = new FileSystemCog();
		}
		return instance;
	}

	public static FileSystemCog newInstance() {
		return new FileSystemCog();
	}

	/**
	 * Creates a file if it does not exist. If the file exists, it is returned.
	 * 
	 * @param filePath
	 *        <span>The target file path.</span>
	 * @return The File object created.
	 */
	public File createFile(String filePath) {
		File file = new File(filePath);
		boolean exists = file.exists();
		if (!exists) {
			try {
				file.createNewFile();
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			} catch (Exception ex) {

			}
		}
		return file;
	}

	/**
	 * Creates a file.
	 * 
	 * @param dirPath
	 * @param fileName
	 * @return
	 */
	public File createFile(String dirPath, String fileName) {
		File dir = new File(dirPath);
		try {
			if (!dir.exists()) {
				dir.mkdirs();
			}
			File file = new File(dir, fileName);
			file.createNewFile();
			return file;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Creates a new file regardless of whether it exists or not. An existing
	 * file would be deleted first.
	 * 
	 * @param filePath
	 * @return Reference to the created file.
	 */
	public File createNewFile(String filePath) {
		File file = new File(filePath);
		try {
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			return file;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Creates a new file regardless of whether it exists or not. An existing
	 * file would be deleted first.
	 * 
	 * @param dirPath
	 *        <span>The target directory path. If it does not exist, it is
	 *        created.</span>
	 * @param fileName
	 *        <span>The target file path.</span>
	 * @return The File object created.
	 */
	public File createNewFile(String dirPath, String fileName) {
		File dir = new File(dirPath);
		try {
			if (!dir.exists() || !dir.isDirectory()) {
				dir.mkdirs();
			}

			File file = new File(dir, fileName);
			if (file.exists()) {
				file.delete();
			}
			file.createNewFile();
			return file;
		} catch (IOException e) {
			//Log.d("CogFileUtil.createFreshFile", e.getMessage());
			// toast(e.getMessage());
			return null;
		}
	}

	/**
	 * Creates a directory if it does not exist. If it exists, it is returned.
	 * 
	 * @param dirPath
	 *        <span>The directory path.</span>
	 * @return The File object created.
	 */
	public File createDirectory(String dirPath) {
		return createDirectory(new File(dirPath));
	}

	/**
	 * Creates a directory if it does not exist. If it exists, it is returned.
	 * 
	 * @param dir
	 *        <span>The directory path.</span>
	 * @return The File object created.
	 */
	public File createDirectory(File dir) {
		if (!dir.exists()) {
			dir.mkdirs();
		}
		return dir;
	}

	/**
	 * Creates a new directory regardless of whether it exists or not.
	 * 
	 * @param dirPath
	 *        <span>The directory path</span>
	 * @return The File object created.
	 */
	public File createNewDirectory(String dirPath) {
		return createNewDirectory(new File(dirPath));
	}

	/**
	 * Creates a new directory regardless of whether it exists or not.
	 * 
	 * @param dir
	 *        <span>The directory to create.</span>
	 * @return The File object created.
	 */
	public File createNewDirectory(File dir) {
		File temp = dir;
		if (dir.exists() && dir.isDirectory()) {
			deleteDirectory(dir.getPath());
		}
		temp.mkdirs();
		return temp;
	}

	public boolean deleteDirectory(String path) {
		return deleteDirectory(new File(path));
	}

	public boolean deleteDirectory(File dir) {
		if (!dir.exists() || !dir.isDirectory())
			return true;

		if (hasFiles(dir)) {
			deleteFilesIn(dir);
		}

		if (hasFolders(dir)) {
			List<File> folders = getFoldersIn(dir);
			for (File folder : folders) {
				deleteDirectory(folder);
			}
		}

		return dir.delete();
	}

	public void purgeDirectory(String dirPath) {
		purgeDirectory(new File(dirPath));
	}

	public void purgeDirectory(File dir) {
		if (!dir.exists() || !dir.isDirectory())
			return;

		if (!isEmpty(dir)) {
			if (hasFiles(dir)) {
				deleteFilesIn(dir);
			}
			if (hasFolders(dir)) {
				deleteFoldersIn(dir);
			}
		}
	}

	/**
	 * Deletes a file.
	 * 
	 * @param file
	 * @return
	 */
	public boolean deleteFile(File file) {
		return file.delete();
	}

	public boolean isEmpty(String path) {
		return isEmpty(new File(path));
	}

	public boolean isEmpty(File fileInstance) {
		if (fileInstance.isDirectory()) {
			validateDir(fileInstance);
			return fileInstance.list().length == 0;
		}

		if (fileInstance.isFile()) {
			validateFile(fileInstance);
			return fileInstance.length() == 0;
		}

		throw new Error("Could not check empty status.");
	}

	public List<File> getFilesIn(String dirPath) {
		return getFilesIn(new File(dirPath));
	}

	public List<File> getFilesIn(File dir) {
		validateDir(dir);

		File[] allContents = dir.listFiles();
		List<File> files = new ArrayList<File>();
		for (File content : allContents) {
			if (content.isFile()) {
				files.add(content);
			}
		}
		return files;
	}

	public List<File> getFoldersIn(String dirPath) {
		return getFoldersIn(new File(dirPath));
	}

	public List<File> getFoldersIn(File dir) {
		validateDir(dir);

		File[] allContents = dir.listFiles();
		List<File> folders = new ArrayList<File>();
		for (File content : allContents) {
			if (content.isDirectory()) {
				folders.add(content);
			}
		}
		return folders;
	}

	public boolean hasFiles(String dirPath) {
		return hasFiles(new File(dirPath));
	}

	public boolean hasFiles(File dir) {
		validateDir(dir);
		return getFilesIn(dir).size() > 0;
	}

	public boolean hasFolders(String dirPath) {
		return hasFolders(new File(dirPath));
	}

	public boolean hasFolders(File dir) {
		validateDir(dir);
		return getFoldersIn(dir).size() > 0;
	}

	public boolean deleteFilesIn(String dirPath) {
		File dir = new File(dirPath);
		return deleteFilesIn(dir);
	}

	public boolean deleteFilesIn(File dir) {
		validateDir(dir);

		boolean flag = false;
		if (dir.exists() && dir.isDirectory()) {
			if (!isEmpty(dir)) {
				List<File> filesIn = getFilesIn(dir);
				for (File file : filesIn) {
					flag = file.delete();
				}
			}
		}
		return flag;
	}

	public boolean deleteFoldersIn(String dirPath) {
		return deleteFoldersIn(new File(dirPath));
	}

	public boolean deleteFoldersIn(File dir) {
		validateDir(dir);

		boolean flag = false;
		if (!isEmpty(dir)) {
			List<File> folders = getFoldersIn(dir);
			for (File folder : folders) {
				flag = deleteDirectory(folder);
			}
		}
		return flag;
	}

	public void writeToFile(File file, String contents) {
		OutputStream outStream = null;
		OutputStreamWriter outWriter = null;
		try {
			outStream = new FileOutputStream(file.getAbsolutePath());
			outWriter = new OutputStreamWriter(outStream);
			outWriter.write(contents);
			outWriter.flush();
			outWriter.close();
		} catch (FileNotFoundException e) {} catch (IOException e) {} finally {
			try {
				outStream.close();
				outWriter.close();
			} catch (Exception ex) {}
		}
	}

	public void writeToFile(File file, byte[] contents) {
		try {
			if (file.exists()) {
				OutputStream outStream = new FileOutputStream(file.getAbsolutePath());
				outStream.write(contents);
				outStream.flush();
				outStream.close();
			}
		} catch (FileNotFoundException e) {} catch (IOException e) {}
	}

	public String readFile(File file) {
		validateFile(file);

		String contents;
		InputStream inStream = null;
		InputStreamReader inReader = null;
		try {
			inStream = new FileInputStream(file);
			inReader = new InputStreamReader(inStream);
			contents = new InputStreamUtil(inReader).getContents();
			return contents;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				inStream.close();
				inReader.close();
			} catch (IOException ex) {}
		}
	}

	//TODO method: copy(File sourceDir, File destDir)...
	public void copy(File sourceFile, File destFile) {
		if (destFile.exists()) {
			destFile.delete();
		}

		try {
			destFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		if (!sourceFile.exists() || !sourceFile.isFile())
			return;

		FileInputStream inStream = null;
		FileOutputStream outStream = null;

		try {
			inStream = new FileInputStream(sourceFile);
			byte[] contents = new byte[(int) sourceFile.length()];
			inStream.read(contents);
			outStream = new FileOutputStream(destFile);
			outStream.write(contents);
			outStream.flush();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		} finally {
			try {
				inStream.close();
				outStream.close();
			} catch (Exception ex) {}
		}
	}

	private void validateDir(File dir) {
		if (!dir.exists() || !dir.isDirectory()) {
			throw new RuntimeException(new FileNotFoundException("No such directory."));
		}
	}

	private void validateFile(File file) {
		if (!file.exists() || !file.isFile()) {
			throw new RuntimeException(new FileNotFoundException("No such file."));
		}
	}
}
