package cog.java.io;

import java.io.InputStreamReader;
import java.io.IOException;

/**
 * Utility class for performing input stream functions.
 * 
 * @author Ikenna Agbasimalo
 */
public class InputStreamUtil {

	private InputStreamReader _inReader;

	/**
	 * Constructor. Sets an InputStreamReader object to be used in this class's
	 * method calls.
	 * 
	 * @param isr
	 *        <span>The InputStreamReader object.</span>
	 */
	public InputStreamUtil(InputStreamReader isr) {
		_inReader = isr;
	}

	/**
	 * Gets the contents of the last InputStreamReader object used with one of
	 * this class's methods, or the one set via a call to
	 * setInputStreamReader(InputStreamReader isr)
	 * 
	 * @return
	 */
	public String getContents() {
		//_inReader = inReader;
		String contents = "";
		final int BLOCKS_TO_READ = 1024;
		char[] characterBuffer = new char[BLOCKS_TO_READ];
		String stringBuffer;
		int numCharsRead;

		try {
			while ((numCharsRead = _inReader.read(characterBuffer)) > 0) {
				stringBuffer = String.copyValueOf(characterBuffer, 0, numCharsRead);
				contents += stringBuffer;
				/*
				 * reset the inputBufferArray as it would have been written to by the
				 * inReader.read() folder.
				 */
				characterBuffer = new char[BLOCKS_TO_READ];
			}
			// file has finished reading...
			return contents;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return "";
		}
	}

}
