package cog.java.util;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Utility class to define an action to be run after an elapsed time in seconds
 * and/or after certain repeated time intervals in seconds.
 * 
 * @author Ikmich
 * 
 */
public class TimerUtil {
	private Timer _timer;
	private double _timeoutSeconds = 0.0;
	private double _intervalSeconds = 0.0;
	private Runnable _actionOnTimeout = null;
	private Runnable _actionOnInterval = null;
	private int _msElapsed = 0;
	private static final long MS_INTERVAL = 1000;

	/**
	 * Constructor
	 */
	public TimerUtil() {

	}

	/**
	 * Sets the time in seconds after which the timer will stop ticking.
	 * 
	 * @param timeoutSeconds
	 * @return
	 */
	public TimerUtil setTimeout(double timeoutSeconds) {
		return setTimeout(timeoutSeconds, null);
	}

	/**
	 * Sets the time in seconds after which the timer will stop ticking and
	 * execute a Runnable action if provided.
	 * 
	 * @param timeoutSeconds
	 * @param actionOnTimeout
	 * @return
	 */
	public TimerUtil setTimeout(double timeoutSeconds, Runnable actionOnTimeout) {
		_timeoutSeconds = timeoutSeconds;
		_actionOnTimeout = actionOnTimeout;
		return this;
	}

	/**
	 * Sets the interval in seconds for the timer to tick.
	 * 
	 * @param intervalSeconds
	 * @return
	 */
	public TimerUtil setInterval(double intervalSeconds) {
		return setInterval(intervalSeconds, null);
	}

	/**
	 * Sets the interval in seconds for the timer to tick. Also sets a Runnable
	 * action to execute at every interval.
	 * 
	 * @param intervalSeconds
	 * @param actionOnInterval
	 * @return
	 */
	public TimerUtil setInterval(double intervalSeconds, Runnable actionOnInterval) {
		_intervalSeconds = intervalSeconds;
		_actionOnInterval = actionOnInterval;
		return this;
	}

	/**
	 * Starts the timer.
	 */
	public void start() {
		final long ms_interval = Math.round(_intervalSeconds > 0.0 ? _intervalSeconds * 1000 : MS_INTERVAL);

		_timer = new Timer();
		_timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				_msElapsed += ms_interval;

				if (_actionOnInterval != null) {
					_actionOnInterval.run();
				}

				if (_timeoutSeconds > 0) {
					if (_msElapsed / 1000 >= _timeoutSeconds) {
						stop();
						if (_actionOnTimeout != null) {
							_actionOnTimeout.run();
						}
					}
				}
			}
		}, ms_interval, ms_interval);
	}

	/*
	 * Stops the timer.
	 */
	public TimerUtil stop() {
		if (_timer != null) {
			_timer.cancel();
		}
		return this;
	}

	/**
	 * Resets the TimerUtil instance.
	 * 
	 * @return
	 */
	public TimerUtil reset() {
		_timer = null;
		_timeoutSeconds = 0.0;
		_intervalSeconds = 0.0;
		_actionOnTimeout = null;
		_actionOnInterval = null;
		_msElapsed = 0;
		return this;
	}
}
