package cog.java.util;

public class RandomUtil {
	/**
	 * Gets random number between two numbers.
	 * 
	 * @param start
	 *        The number to start from.
	 * @param end
	 *        The number to end from.
	 * @return A random number that is either <b>start</b>, <b>end</b>, or a
	 *         number in-between.
	 */
	public static int getRandom(int start, int end) {
		int r = start + (int) (Math.random() * ((end - start) + 1));
		return r;
	}
}
