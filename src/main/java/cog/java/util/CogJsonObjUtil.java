package cog.java.util;

import org.json.JSONException;
import org.json.JSONObject;

public class CogJsonObjUtil {
	private JSONObject jsonObj;

	public CogJsonObjUtil(JSONObject jsonObj) {
		this.jsonObj = jsonObj;
	}

	public String getStringOrNull(String key) {
		try {
			Object obVal = jsonObj.opt(key);
			if (!obVal.equals(JSONObject.NULL)) {
				String s = obVal.toString();
				if (!s.equals("null")) {
					return jsonObj.getString(key);
				}
				return null;
			}
			return null;
		} catch (NullPointerException e) {
			return null;
		} catch (JSONException e) {
			return null;
		}
	}

	public JSONObject getJsonObjectOrNull(String key) {
		try {
			Object obVal = jsonObj.opt(key);
			if (!obVal.equals(JSONObject.NULL)) {
				String s = obVal.toString();
				if (!s.equals("null")) {
					return jsonObj.getJSONObject(key);
				}
				return null;
			}
			return null;
		} catch (NullPointerException e) {
			return null;
		} catch (JSONException e) {
			return null;
		}
	}
}
