package cog.java.util;

public class Toggler {
	private boolean on = false;

	public Toggler() {}

	public boolean isOn() {
		return on;
	}

	public boolean isOff() {
		return !on;
	}

	public void toggle() {
		if (on) {
			on = false;
		}
		else {
			on = true;
		}
	}

	public void toggle(Runnable r1, Runnable r2) {
		if (on) {
			r2.run();
			on = false;
		}
		else {
			r1.run();
			on = true;
		}
	}
}
