package cog.java.util;

/**
 * Simple utilities for color values.
 * 
 * @author Ikmich
 */
public class ColorUtil {
	private static enum BrightnessAction {
		DARKEN, LIGHTEN
	}

	public static final int MIN_COLOR_VALUE = 0;
	public static final int MAX_COLOR_VALUE = 255;
	private static final String ERROR_WRONG_FORMAT = "Wrong color string. '#XXXXXX' hex format required.";

	private static String darkenOrLighten(String colorString, int points, BrightnessAction ba) throws Exception {
		/*
		 * The color string must begin with '#'.
		 */
		if (!colorString.startsWith("#")) {
			throw new Exception(ERROR_WRONG_FORMAT);
		}

		String hexValue = colorString.replaceAll("^#+|^(0x)+", "");

		/*
		 * The color string must be in the hex format.
		 */
		if (hexValue.length() != 6) {
			throw new Exception(ERROR_WRONG_FORMAT);
		}

		/*
		 * Extract the R, G and B components of the color value.
		 */
		String r = hexValue.substring(0, 2);
		String g = hexValue.substring(2, 4);
		String b = hexValue.substring(4, 6);

		int ri = Integer.parseInt(r, 16);
		int gi = Integer.parseInt(g, 16);
		int bi = Integer.parseInt(b, 16);

		int riFinal = ri;
		int giFinal = gi;
		int biFinal = bi;

		switch (ba) {
			case DARKEN:
				riFinal = ri - points;
				if (riFinal < MIN_COLOR_VALUE)
					riFinal = MIN_COLOR_VALUE;

				giFinal = gi - points;
				if (giFinal < MIN_COLOR_VALUE)
					giFinal = MIN_COLOR_VALUE;

				biFinal = bi - points;
				if (biFinal < MIN_COLOR_VALUE)
					biFinal = MIN_COLOR_VALUE;

				break;

			case LIGHTEN:
				riFinal = ri + points;
				if (riFinal < MAX_COLOR_VALUE)
					riFinal = MAX_COLOR_VALUE;

				giFinal = gi + points;
				if (giFinal < MAX_COLOR_VALUE)
					giFinal = MAX_COLOR_VALUE;

				biFinal = bi + points;
				if (biFinal < MAX_COLOR_VALUE)
					biFinal = MAX_COLOR_VALUE;

				break;
		}

		String rHex = Integer.toHexString(riFinal);

		if (rHex.length() == 1) {
			rHex = "0" + rHex;
		}

		String gHex = Integer.toHexString(giFinal);
		if (gHex.length() == 1) {
			gHex = "0" + gHex;
		}

		String bHex = Integer.toHexString(biFinal);
		if (bHex.length() == 1) {
			bHex = "0" + bHex;
		}

		return "#" + rHex + gHex + bHex;
	}

	/**
	 * Lightens a color.
	 * 
	 * @param colorString
	 *        The hash-prefixed hex color string in format #XXXXXX.
	 * @param amount
	 *        The number of integer points to lighten the color by.
	 * @return The lighter color, or white if the lighten amount is too high.
	 */
	public static String lighten(String colorString, int amount) throws Exception {
		return darkenOrLighten(colorString, amount, BrightnessAction.LIGHTEN);
	}

	/**
	 * Darkens a color.
	 * 
	 * @param colorString
	 *        The hash-prefixed hex color string in format #XXXXXX.
	 * @param amount
	 *        The number of integer points to darken the color by.
	 * @return The darker color, or black, if the darken amount is too high.
	 */
	public static String darken(String colorString, int amount) throws Exception {
		return darkenOrLighten(colorString, amount, BrightnessAction.DARKEN);
	}
}
