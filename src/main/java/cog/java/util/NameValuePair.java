package cog.java.util;

public class NameValuePair {
	protected String name;
	protected Object value;

	public NameValuePair() {}

	public NameValuePair(String name, Object value) {
		this.name = name;
		this.value = value;
	}

	public NameValuePair setName(String name) {
		this.name = name;
		return this;
	}

	public NameValuePair setValue(String value) {
		this.value = value;
		return this;
	}

	public String getName() {
		return this.name;
	}

	public Object getValue() {
		return this.value;
	}
}
