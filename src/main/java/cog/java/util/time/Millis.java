package cog.java.util.time;

public class Millis {
	public static double toSec(double millis) {
		return millis / Sec.MS_IN_SEC;
	}

	public static double toMin(double millis) {
		return toSec(millis) / Min.SECS_IN_MIN;
	}

	public static double toHour(double millis) {
		return toMin(millis) / Hour.MINS_IN_HOUR;
	}

	public static double toDay(double millis) {
		return toHour(millis) / Day.HOURS_IN_DAY;
	}

	public static double toWeek(double millis) {
		return toDay(millis) / Week.DAYS_IN_WEEK;
	}
}
