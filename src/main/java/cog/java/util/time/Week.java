package cog.java.util.time;

public class Week {
	public static final int DAYS_IN_WEEK = 7;

	public static double toDay(double week) {
		return week * DAYS_IN_WEEK;
	}

	public static double toHour(double week) {
		return toDay(week) * Day.HOURS_IN_DAY;
	}

	public static double toMin(double week) {
		return toHour(week) * Hour.MINS_IN_HOUR;
	}

	public static double toSec(double week) {
		return toMin(week) * Min.SECS_IN_MIN;
	}

	public static double toMillis(double week) {
		return toSec(week) * Sec.MS_IN_SEC;
	}
}
