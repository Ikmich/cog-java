package cog.java.util.time;

public class Sec {
	public static final int MS_IN_SEC = 1000;

	public static double toMillis(double sec) {
		return sec * MS_IN_SEC;
	}

	public static double toMin(double sec) {
		return sec / Min.SECS_IN_MIN;
	}

	public static double toHour(double sec) {
		return toMin(sec) / Hour.MINS_IN_HOUR;
	}
}
