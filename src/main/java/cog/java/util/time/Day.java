package cog.java.util.time;

public class Day {
	public static final int HOURS_IN_DAY = 24;

	public static double toHour(double day) {
		return day * HOURS_IN_DAY;
	}

	public static double toMin(double day) {
		return toHour(day) * Hour.MINS_IN_HOUR;
	}

	public static double toSec(double day) {
		return toMin(day) * Min.SECS_IN_MIN;
	}

	public static double toMillis(double day) {
		return toSec(day) * Sec.MS_IN_SEC;
	}

}
