package cog.java.util.time;

public class Hour {
	public static final int MINS_IN_HOUR = 60;

	public static double toMin(double hour) {
		return hour * MINS_IN_HOUR;
	}

	public static double toSec(double hour) {
		return toMin(hour) * Min.SECS_IN_MIN;
	}

	public static double toMillis(double hour) {
		return toSec(hour) * Sec.MS_IN_SEC;
	}

	public static double toDay(double hour) {
		return hour / Day.HOURS_IN_DAY;
	}

	public static double toWeek(double hour) {
		return toDay(hour) / Week.DAYS_IN_WEEK;
	}
}
