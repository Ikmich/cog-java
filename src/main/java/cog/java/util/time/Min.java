package cog.java.util.time;

/**
 * Utility for working with minute unit of time.
 * 
 * @author Ikmich
 */
public class Min {
	public static final int SECS_IN_MIN = 60;

	public static double toSec(double min) {
		return min * SECS_IN_MIN;
	}

	public static double toMillis(double min) {
		return toSec(min) * Sec.MS_IN_SEC;
	}

	public static double toHour(double min) {
		return min / Hour.MINS_IN_HOUR;
	}

	public static double toDay(double min) {
		return toHour(min) / Day.HOURS_IN_DAY;
	}

	public static double toWeek(double min) {
		return toDay(min) / Week.DAYS_IN_WEEK;
	}
}
