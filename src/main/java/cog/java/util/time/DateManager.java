package cog.java.util.time;

import java.util.Calendar;
import java.util.Date;

/**
 * Utility class for working with dates.
 * 
 * @author Ikmich
 */
public class DateManager {
	private Calendar _cal;
	public final static String[] monthNames = { "January", "February", "March", "April", "May", "June", "July",
		"August", "September", "October", "November", "December" };
	public final static String[] monthNames_short = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
		"Oct", "Nov", "Dec" };
	public final static String[] dayNames = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
		"Saturday" };

	public final static String[] dayNames_short = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

	/**
	 * Create a DateManager instance using the current time to create the
	 * internal Calendar object.
	 */
	public DateManager() {
		this(Calendar.getInstance());
	}

	/**
	 * Create a DateManager instance using a specified Calendar object.
	 * 
	 * @param cal
	 */
	public DateManager(Calendar cal) {
		_cal = cal;
	}

	/**
	 * Create a DateManager instance using a time value in milliseconds to
	 * create the internal Calendar object.
	 * 
	 * @param timeInMillis
	 */
	public DateManager(long timeInMillis) {
		_cal = Calendar.getInstance();
		_cal.setTimeInMillis(timeInMillis);
	}

	/**
	 * Sets the DateManager to a 'fresh' state without having to initialize via
	 * a constructor.
	 */
	public void init() {
		init(Calendar.getInstance());
	}

	/**
	 * Sets the DateManager to a 'fresh' state without having to initialize via
	 * a constructor.
	 * 
	 * @param cal
	 *        A Calendar object to initialize the internal Calendar object with.
	 */
	public void init(Calendar cal) {
		_cal = cal;
	}

	/**
	 * Sets the DateManager to a 'fresh' state without having to initialize via
	 * a constructor.
	 * 
	 * @param timeInMillis
	 *        The milliseconds time to initialize the internal Calendar object
	 *        with.
	 */
	public void init(long timeInMillis) {
		_cal = Calendar.getInstance();
		_cal.setTimeInMillis(timeInMillis);
	}

	public void setYear(int year) {
		_cal.set(Calendar.YEAR, year);
	}

	public int getYear() {
		return _cal.get(Calendar.YEAR);
	}

	public void setMonth(int month) {
		_cal.set(Calendar.MONTH, month);
	}

	public int getMonth() {
		return _cal.get(Calendar.MONTH);
	}

	public String getMonth(boolean zeroPrefix) {
		int month = getMonth();
		if (zeroPrefix && month < 10) {
			return "0" + month;
		}
		return String.valueOf(month);
	}

	public String getFullMonth() {
		return monthNames[_cal.get(Calendar.MONTH)];
	}

	public static String getFullMonth(int month) {
		if (month < 0)
			month = 0;
		if (month > 11)
			month = 11;

		return monthNames[month];
	}

	public String getShortMonth() {
		return monthNames_short[_cal.get(Calendar.MONTH)];
	}

	public static String getShortMonth(int month) {
		if (month < 0)
			month = 0;
		if (month > 11)
			month = 11;

		return monthNames_short[month];
	}

	public void setDay(int day) {
		_cal.set(Calendar.DAY_OF_MONTH, day);
	}

	public int getDay() {
		return _cal.get(Calendar.DAY_OF_MONTH);
	}

	public String getDay(boolean zeroPrefix) {
		int day = getDay();
		if (zeroPrefix && day < 10) {
			return "0" + day;
		}
		return String.valueOf(day);
	}

	public int getDayOfYear() {
		return _cal.get(Calendar.DAY_OF_YEAR);
	}

	public String getDayOfYear(boolean zeroPrefix) {
		int day = getDayOfYear();
		if (zeroPrefix && day < 10) {
			return "0" + day;
		}
		return String.valueOf(day);
	}

	public int toHour24(int hour, int am_pm) {
		if (hour < 1)
			hour = 1;
		if (hour > 12)
			hour = 12;

		if (am_pm < Calendar.AM)
			am_pm = Calendar.AM;
		if (am_pm > Calendar.PM)
			am_pm = Calendar.PM;

		int hour24 = 0;

		switch (am_pm) {
			case Calendar.AM:
				hour24 = (hour == 12) ? 0 : hour;
				break;
			case Calendar.PM:
				hour24 = (hour == 12) ? 12 : hour + 12;
				break;
		}

		return hour24;
	}

	public int toHour12(int hour24) {
		if (hour24 < 0)
			hour24 = 0;
		if (hour24 > 23)
			hour24 = 23;

		int hour12 = 0;

		if (hour24 >= 0 && hour24 <= 11) {
			hour12 = (hour24 == 0) ? 12 : hour24;
		}
		else if (hour24 >= 12 && hour24 <= 23) {
			hour12 = (hour24 == 12) ? 12 : (hour24 - 12);
		}

		return hour12;
	}

	public void setHour(int hour, int am_pm) {
		if (hour < 0)
			hour = 0;
		if (hour > 23)
			hour = 23;

		/*
		 * If the hour value passed is greater than 12 or equal to
		 * 0, assume that user wants to set the hour of day (24-hour clock).
		 * Thus, ignore the 'am_pm' parameter.
		 */
		if (hour > 12 || hour == 0) {
			setHourOfDay(hour);
			return;
		}

		if (am_pm < Calendar.AM)
			am_pm = Calendar.AM;
		if (am_pm > Calendar.PM)
			am_pm = Calendar.PM;

		int hour24 = toHour24(hour, am_pm);
		_cal.set(Calendar.HOUR_OF_DAY, hour24);
	}

	/**
	 * Sets the hour of the day (0 - 23).
	 * 
	 * @param hour24
	 */
	public void setHourOfDay(int hour24) {
		if (hour24 < 0)
			hour24 = 0;
		if (hour24 > 23)
			hour24 = 23;

		_cal.set(Calendar.HOUR_OF_DAY, hour24);
	}

	/**
	 * Gets the current hour (1 - 12).
	 * 
	 * @return
	 */
	public int getHour() {
		int h = _cal.get(Calendar.HOUR);
		if (h == 0)
			h = 12;
		return h;
	}

	/**
	 * Gets the current Hour (1 - 12).
	 * 
	 * @param zeroPrefix
	 * @return
	 */
	public String getHour(boolean zeroPrefix) {
		int hour = getHour();
		if (zeroPrefix && hour < 10) {
			return "0" + hour;
		}
		return hour + "";
	}

	/**
	 * Gets the hour of day (0 - 12).
	 * 
	 * @return
	 */
	public int getHour24() {
		return _cal.get(Calendar.HOUR_OF_DAY);
	}

	public int getHourOfDay() {
		return getHour24();
	}

	public String getHour24(boolean zeroPrefix) {
		int hour = getHour24();
		if (zeroPrefix && hour < 10) {
			return "0" + hour;
		}
		return String.valueOf(hour);
	}

	public String getHourOfDay(boolean zeroPrefix) {
		return getHour24(zeroPrefix);
	}

	public void setMinute(int minute) {
		_cal.set(Calendar.MINUTE, minute);
	}

	public int getMinute() {
		return _cal.get(Calendar.MINUTE);
	}

	public String getMinute(boolean zeroPrefix) {
		int min = getMinute();
		if (zeroPrefix && min < 10) {
			return "0" + min;
		}

		return String.valueOf(min);
	}

	public void setSeconds(int seconds) {
		_cal.set(Calendar.SECOND, seconds);
	}

	public int getSeconds() {
		return _cal.get(Calendar.SECOND);
	}

	public String getSeconds(boolean zeroPrefix) {
		int sec = getSeconds();
		if (zeroPrefix && sec < 10) {
			return "0" + sec;
		}
		return String.valueOf(sec);
	}

	public String getMeridien() {
		switch (_cal.get(Calendar.AM_PM)) {
			case 0:
				return "am";
			case 1:
				return "pm";
			default:
				return "";
		}
	}

	public String getFriendlyTimeString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getHour()).append(":");
		sb.append(getMinute(true)).append(":");
		sb.append(getSeconds(true));
		sb.append(getMeridien());

		return sb.toString();
	}

	public String getFriendlyTimeString(boolean h24) {
		StringBuilder sb = new StringBuilder();
		sb.append(getHour24(true)).append(":");
		sb.append(getMinute(true)).append(":");
		sb.append(getSeconds(true));
		sb.append(getMeridien());

		return sb.toString();
	}

	public String getFriendlyDateString() {
		return getFriendlyDateString(null, false, true);
	}

	public String getFriendlyDateString(boolean showTime) {
		return getFriendlyDateString(null, false, showTime);
	}

	public String getFriendlyDateString(String dateAndTimeSeparator) {
		return getFriendlyDateString(dateAndTimeSeparator, false, true);
	}

	public String getFriendlyDateString(String dateAndTimeSeparator, boolean useFriendlySuffix) {
		return getFriendlyDateString(dateAndTimeSeparator, useFriendlySuffix, true);
	}

	public String getFriendlyDateString(String dateAndTimeSeparator, boolean useFriendlySuffix, boolean showTime) {
		StringBuilder sb = new StringBuilder();

		/*
		 * Day
		 */
		String sDay = dayNames[_cal.get(Calendar.DAY_OF_WEEK) - 1].substring(0, 3);
		sb.append(sDay);
		//sb.append(".");

		/*
		 * Month
		 */
		String sMonth = monthNames[_cal.get(Calendar.MONTH)].substring(0, 3);
		sb.append(" ").append(sMonth).append(".");

		/*
		 * Day
		 */
		int iDay = _cal.get(Calendar.DAY_OF_MONTH);
		sb.append(" ").append(iDay).append(",");

		/*
		 * Year
		 */
		sb.append(" ").append(_cal.get(Calendar.YEAR));

		if (showTime) {
			if (dateAndTimeSeparator == null) {
				dateAndTimeSeparator = ";";
			}
			else {
				dateAndTimeSeparator = " " + dateAndTimeSeparator.trim();
			}

			sb.append(dateAndTimeSeparator);

			/*
			 * Hour
			 */
			int iHour = _cal.get(Calendar.HOUR);
			if (iHour == 0) {
				iHour = 12;
			}
			String sHour = iHour < 10 ? "0" + iHour : iHour + "";
			sb.append(" ").append(sHour);

			/*
			 * Minute
			 */
			int iMinute = _cal.get(Calendar.MINUTE);
			String sMinute = iMinute < 10 ? "0" + iMinute : iMinute + "";
			sb.append(":").append(sMinute);

			//		int iSeconds = _cal.get(Calendar.SECOND);
			//		String sSeconds = iSeconds < 10 ? "0" + iSeconds : iSeconds + "";
			//		sb.append(":").append(sSeconds);

			/*
			 * Meridien
			 */
			int iAmPm = _cal.get(Calendar.AM_PM);
			String sAmPm = iAmPm == 0 ? "AM" : "PM";
			sb.append(" ").append(sAmPm);
		}

		if (useFriendlySuffix) {
			/*
			 * Today or tomorrow or yesterday
			 */
			if (isTomorrow()) {
				sb.append(" (TOMORW.)");
			}

			else if (isToday()) {
				sb.append(" (TODAY)");
			}

			else if (isYesterday()) {
				sb.append(" (YESTERDAY)");
			}
		}

		return sb.toString();
	}

	public boolean isToday() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.DAY_OF_YEAR) == _cal.get(Calendar.DAY_OF_YEAR);
	}

	public boolean isTomorrow() {
		return Calendar.getInstance().get(Calendar.DAY_OF_YEAR) == (_cal.get(Calendar.DAY_OF_YEAR) - 1);
	}

	public boolean isYesterday() {
		return Calendar.getInstance().get(Calendar.DAY_OF_YEAR) == (_cal.get(Calendar.DAY_OF_YEAR) + 1);
	}

	public long getTimeInMillis() {
		return _cal.getTimeInMillis();
	}

	public Date getDate() {
		return _cal.getTime();
	}

	public Calendar getCalendarObject() {
		return _cal;
	}

	public int compare(DateManager dm) {
		return compareParts(this.getTimeInMillis(), dm.getTimeInMillis());
	}

	public int compareAtYearLevel(DateManager dm) {
		return compareParts(this.getYear(), dm.getYear());
	}

	public int compareAtMonthLevel(DateManager dm) {
		if (compareAtYearLevel(dm) == 1) {
			return 1;
		}

		if (compareAtYearLevel(dm) == 0) {
			return compareParts(this.getMonth(), dm.getMonth());
		}

		return -1;
	}

	public int compareAtDayLevel(DateManager dm) {
		if (compareAtMonthLevel(dm) == 1) {
			return 1;
		}

		if (compareAtMonthLevel(dm) == 0) {
			return compareParts(this.getDay(), dm.getDay());
		}

		return -1;
	}

	public int compareAtHourLevel(DateManager dm) {
		if (compareAtDayLevel(dm) == 1) {
			return 1;
		}

		if (compareAtDayLevel(dm) == 0) {
			return compareParts(this.getHour24(), dm.getHour24());
		}

		return -1;
	}

	public int compareAtMinuteLevel(DateManager dm) {
		if (compareAtHourLevel(dm) == 1) {
			return 1;
		}

		if (compareAtHourLevel(dm) == 0) {
			return compareParts(this.getMinute(), dm.getMinute());
		}

		return -1;
	}

	protected int compareParts(long p1, long p2) {
		if (p1 > p2) {
			return 1;
		}

		if (p1 < p2) {
			return -1;
		}

		return 0;
	}

}
