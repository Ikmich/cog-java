package cog.java.xml;

import java.io.File;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cog.java.util.NameValuePair;

public class XmlDocumentMaker {
	DocumentBuilderFactory docBuilderFactory;
	DocumentBuilder docBuilder;
	Document doc;
	TransformerFactory transformerFactory;
	Transformer transformer;

	String rootElementName;
	Element rootElement;

	public XmlDocumentMaker(String rootElem) {
		this.rootElementName = rootElem;
		docBuilderFactory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			rootElement = doc.createElement(this.rootElementName);
			doc.appendChild(rootElement);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		try {
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Adds an element to the root element.
	 * 
	 * @param name
	 */
	public Element addElement(String name) {
		return addElement(rootElement, name);
	}

	public Element addElement(String name, NameValuePair[] attrs) {
		return addElementTo(rootElement, name, attrs);
	}

	/**
	 * Adds an element to an element.
	 * 
	 * @param parentElement
	 * @param name
	 */
	public Element addElement(Element parentElement, String name) {
		Element elem = doc.createElement(name);
		parentElement.appendChild(elem);
		return elem;
	}

	/**
	 * Adds an element to an element.
	 * 
	 * @param parentElement
	 * @param name
	 * @param attrs
	 * @return
	 */
	public Element addElementTo(Element parentElement, String name, NameValuePair[] attrs) {
		Element elem = doc.createElement(name);
		for (int i = 0; i < attrs.length; i++) {
			String attributeName = attrs[i].getName();
			String attributeValue = attrs[i].getValue().toString();
			elem.setAttribute(attributeName, attributeValue);
		}

		parentElement.appendChild(elem);
		return elem;
	}

	/**
	 * Saves the xml document to a file.
	 * 
	 * @param filepath
	 */
	public void saveToFile(String filepath) {
		saveToFile(new File(filepath));
	}

	/**
	 * Saves the XML contents to a file.
	 * 
	 * @param file
	 */
	public void saveToFile(File file) {
		DOMSource domSource = new DOMSource(doc);
		StreamResult sr = new StreamResult(file);
		try {
			transformer.transform(domSource, sr);
		} catch (TransformerException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns the XML contents as text.
	 * 
	 * @return
	 */
	public String getXmlText() {
		DOMSource domSource = new DOMSource(doc);
		StringWriter sw = new StringWriter();
		StreamResult sr = new StreamResult(sw);

		try {
			transformer.transform(domSource, sr);
			String s = sw.getBuffer().toString();
			return s;
		} catch (TransformerException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns this instance's XML document.
	 * 
	 * @return
	 */
	public Document getXmlDoc() {
		return doc;
	}
}
