package cog.java.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Reads xml data from an XML Document instance or an XML file.
 * 
 * @author Ikmich
 * 
 */
public class XmlDocumentReader {
	Document doc;
	DocumentBuilderFactory docBuilderFactory;
	DocumentBuilder docBuilder;

	public XmlDocumentReader(Document xmlDoc) {
		doc = xmlDoc;
	}

	public XmlDocumentReader(File xmlFile) {
		docBuilderFactory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			doc = docBuilder.parse(xmlFile);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (SAXException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets the root element for the document.
	 * 
	 * @return The root element.
	 */
	public Element getRootElement() {
		NodeList nodes = doc.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE)
				return (Element) node;
		}
		throw new RuntimeException("Could not get the root element.");
	}

	/**
	 * Gets the elements in the document.
	 * 
	 * @param tagName
	 * @return
	 */
	public List<Element> getElements(String tagName) {
		return getElementsIn((Object) doc, tagName);
	}

	/**
	 * Gets the elements in an element.
	 * 
	 * @param elem
	 * @param tagName
	 * @return
	 */
	public List<Element> getElementsIn(Element elem, String tagName) {
		return getElementsIn((Object) elem, tagName);
	}

	/**
	 * Gets the elements in an element.
	 * 
	 * @param o
	 * @param tagName
	 * @return
	 */
	private List<Element> getElementsIn(Object o, String tagName) {
		NodeList nodes = null;
		if (o instanceof Document) {
			nodes = ((Document) o).getElementsByTagName(tagName);
		}
		else if (o instanceof Element) {
			nodes = ((Element) o).getElementsByTagName(tagName);
		}

		List<Element> elements = new ArrayList<Element>();
		for (int i = 0; i < nodes.getLength(); i++) {
			elements.add((Element) nodes.item(i));
		}

		return elements;
	}

	/**
	 * Gets the attributes for this element.
	 * 
	 * @param elem
	 * @return
	 */
	public HashMap<String, String> getAttributes(Element elem) {
		HashMap<String, String> attrs;
		//NameValuePair[] attrs;
		NamedNodeMap attributeNodes = elem.getAttributes();
		//attrs = new NameValuePair[attributeNodes.getLength()];
		attrs = new HashMap<String, String>(attributeNodes.getLength());
		for (int i = 0; i < attributeNodes.getLength(); i++) {
			Node attributeNode = attributeNodes.item(i);
			String attributeName = attributeNode.getNodeName();
			String attributeValue = attributeNode.getNodeValue();
			//attrs[i] = new NameValuePair(attributeName, attributeValue);
			attrs.put(attributeName, attributeValue);
		}
		return attrs;
	}
}
