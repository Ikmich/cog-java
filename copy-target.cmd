rem copy cog-java jars to usage libs dest.

set "ver=1.0.5"

set "source1=cog-java-%ver%.jar"
set "source2=cog-java-%ver%-javadoc.jar"
set "source3=cog-java-%ver%-sources.jar"

set "target_dir=C:\konga-projects\android\Lean Delivery App\lean_delivery_app\app\libs"
robocopy ./target "%target_dir%" "%source1%" "%source2%" "%source3%"

rem Copy cog-java jar to Konga Android Shopping App project.
set "target_dir=C:\konga-projects\android\Shopping App\kongashoppingandroid_z\Dev\KongaApp\libs"
robocopy ./target "%target_dir%" "%source1%" "%source2%" "%source3%"

rem Copy cog-java jar to OrganizeYOU app project.
set "target_dir=C:\projects\android\androidstudio\OrganizeYou\app\libs"
robocopy ./target "%target_dir%" "%source1%" "%source2%" "%source3%"
